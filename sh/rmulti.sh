#!/bin/bash
#Init IP Routing in linux kernel
echo 1 > /proc/sys/net/ipv4/ip_forward

#Add network tools calculator
source /home/soft/nettools.sh
source ./nettools.sh

#Load ip configurations
. /home/soft/rmulti_ips.sh
. rmulti_ips.sh


IP1=`getipaddr $IF1`
IP0=`getipaddr $IF0`
IP0_0=`getipaddr $IF0_0`
IP0_1=`getipaddr $IF0_1`
IP0_2=`getipaddr $IF0_2`
IP_TUN=`getipaddr $TUN`
IP_LXCBR=`getipaddr $LXCBR`

IP1bits=`getbits $IF1`
IP0bits=`getbits $IF0`
IP0_0bits=`getbits $IF0_0`
IP0_1bits=`getbits $IF0_1`
IP0_2bits=`getbits $IF0_2`
IP_TUNbits=24

P1_NET=$( network $IP1 $IP1bits )/$IP1bits
P0_NET=$( network $IP0 $IP0bits )/$IP0bits
P0_NET0=$( network $IP0_0 $IP0_0bits )/$IP0_0bits
P0_1_NET=$( network $IP0_1 $IP0_1bits )/$IP0_1bits
P0_2_NET=$( network $IP0_2 $IP0_2bits )/$IP0_2bits
TUN_NET=$( network $IP_TUN $IP_TUNbits )/$IP_TUNbits

W1=3
W2=1

DTIME=`date '+%d.%m.%Y %H:%M:%S'`

#Create tables
rt_tables="/etc/iproute2/rt_tables";
if [ `grep "T1" $rt_tables|grep -v grep|wc -l` -eq 0 ];
then
echo "$DTIME T1 doesn't exist. Creating T1."
echo "200 T1" >> $rt_tables
fi;

if [ `grep "T2" $rt_tables|grep -v grep|wc -l` -eq 0 ];
then
echo "$DTIME T2 doesn't exist. Creating T2."
echo "201 T2" >> $rt_tables
fi;


#Create TOS - valid 00,02,04,08,10
rt_dsfield="/etc/iproute2/rt_dsfield";
if [ `grep "MTEL" $rt_dsfield|grep -v grep|wc -l` -eq 0 ];
then
echo "$DTIME TOS MTEL doesn't exist. Creating MTEL."
echo "0x02 MTEL" >> $rt_dsfield
fi;

rt_dsfield="/etc/iproute2/rt_dsfield";
if [ `grep "DIANA" $rt_dsfield|grep -v grep|wc -l` -eq 0 ];
then
echo "$DTIME TOS DIANA doesn't exist. Creating DIANA."
echo "0x04 DIANA" >> $rt_dsfield
fi;

echo "IP $IF1 is $IP1 network $P1_NET"
echo "IP $IF0 is $IP0 network $P0_NET"
echo "IP $IF0_1 is $IP0_1 network $P0_1_NET"
echo "IP $IF0_2 is $IP0_2 network $P0_2_NET"
echo "IP $TUN is $IP_TUN network $TUN_NET"
echo "IP $LXCBR is $IP_LXCBR network $LXCBR_NET"
echo ""

function gw1 {
    route del -net 0.0.0.0 netmask 0.0.0.0
    ip route add default scope global via $GW1 dev $IF1
    ip route flush cache
 }

function gw2 {
    route del -net 0.0.0.0 netmask 0.0.0.0
    ip route add default scope global via $GW2 dev $IF0_2
    ip route flush cache
 }

 function rmulti {
    gw1
    policy
 }

function r1 {
    gw1
    rpolicy
 }

function r2 {
    gw2
    rpolicy
 }

function policy {
        #remove previous policies
	iptables -t mangle -F INPUT
	iptables -t mangle -F OUTPUT
	iptables -t mangle -F PREROUTING
        #Restore connection Tracking
        iptables -t mangle -A PREROUTING -i $IF1 -m conntrack --ctstate NEW -j CONNMARK --set-mark 1
        iptables -t mangle -A PREROUTING -i $IF0 ! -s 192.168.0.0/16 -m conntrack --ctstate NEW -j CONNMARK --set-mark 2
        iptables -t mangle -A PREROUTING -i $IF0 -m conntrack --ctstate ESTABLISHED,RELATED -j CONNMARK --restore-mark

   #IpArea1
   len=${#IPAREA1[@]}
   for (( i=0; i<${len}; i++))
      do
            if [[ ${IPAREA1[$i]} = *"-"* ]]; then #- is present so it's range
              STMT="-m iprange --src-range ${IPAREA1[$i]}"
             else
              STMT="--src ${IPAREA1[$i]}"
             fi

           iptables -t mangle -A PREROUTING -i $IF0 -m conntrack --ctstate NEW $STMT -j CONNMARK --set-mark 1
           iptables -t mangle -A PREROUTING -i $IF0 -m conntrack --ctstate NEW $STMT -j MARK --set-mark 1
      done

   #IpArea2
   len=${#IPAREA2[@]}
   for (( i=0; i<${len}; i++))
      do
            if [[ ${IPAREA2[$i]} = *"-"* ]]; then #- is present so it's range
              STMT="-m iprange --src-range ${IPAREA2[$i]}"
             else
              STMT="--src ${IPAREA2[$i]}"
             fi

           iptables -t mangle -A PREROUTING -i $IF0 -m conntrack --ctstate NEW $STMT -j CONNMARK --set-mark 2
           iptables -t mangle -A PREROUTING -i $IF0 -m conntrack --ctstate NEW $STMT -j MARK --set-mark 2
      done

	#autoselection will work on IPDYN ip ranges
	len=${#IPDYN[@]}
	 for (( i=0; i<${len}; i++))
          do
            if [[ ${IPDYN[$i]} = *"-"* ]]; then #- is present so it's range
              STMT="-m iprange --src-range ${IPDYN[$i]}"
             else
              STMT="--src ${IPDYN[$i]}"
             fi

    	   iptables -t mangle -A PREROUTING -i $IF0 $STMT -m state --state NEW -m statistic --mode nth  --every 2 --packet 0 -j MARK --set-mark 1
           iptables -t mangle -A PREROUTING -i $IF0 $STMT -m state --state NEW -m statistic --mode nth  --every 2 --packet 1 -j MARK --set-mark 2
    	   iptables -t mangle -A PREROUTING -i $IF0 $STMT -m state --state NEW -m statistic --mode nth  --every 2 --packet 0 -j CONNMARK --set-mark 1
    	   iptables -t mangle -A PREROUTING -i $IF0 $STMT -m state --state NEW -m statistic --mode nth  --every 2 --packet 1 -j CONNMARK --set-mark 2
          done


        #Flush output and restore connection tracking
        iptables -t mangle -F OUTPUT
        iptables -t mangle -A OUTPUT -m conntrack --ctstate ESTABLISHED,RELATED -j CONNMARK --restore-mark

	#Set ballancing on ooutput packets
        iptables -t mangle -A OUTPUT -m conntrack --ctstate NEW -m statistic --mode nth  --every 2 --packet 0 -j CONNMARK --set-mark 1
        iptables -t mangle -A OUTPUT -m conntrack --ctstate NEW -m statistic --mode nth  --every 2 --packet 1 -j CONNMARK --set-mark 2
        iptables -t mangle -A OUTPUT -m state --state NEW -m statistic --mode nth  --every 2 --packet 0 -j MARK --set-mark 1
        iptables -t mangle -A OUTPUT -m state --state NEW -m statistic --mode nth  --every 2 --packet 1 -j MARK --set-mark 2
        #Balance internet connections

        iptables -t mangle -A PREROUTING -i $IF0 -m state --state NEW -j CONNMARK --save-mark
        iptables -t mangle -A PREROUTING -s 127.0.0.0/24 -m state --state NEW -j CONNMARK --save-mark

        #IpArea 1 & IpArea 2
        #IpArea 1
 }

function rpolicy {
	iptables -t mangle -F PREROUTING
        #Restore connection Tracking
        iptables -t mangle -A PREROUTING -i $IF1 -m conntrack --ctstate NEW -j CONNMARK --set-mark 1
        iptables -t mangle -A PREROUTING -i $IF0_2 -m conntrack --ctstate NEW -j CONNMARK --set-mark 2
        iptables -t mangle -A PREROUTING -i $IF0 -m conntrack --ctstate ESTABLISHED,RELATED -j CONNMARK --restore-mark
        #Flush output and restore connection tracking
        iptables -t mangle -F OUTPUT
        iptables -t mangle -A OUTPUT -m conntrack --ctstate ESTABLISHED,RELATED -j CONNMARK --restore-mark
 }

function ddosProt {
  #Drop invalid packets
  iptables -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP

  #Block Packets From Private Subnets (Spoofing)
  iptables -t mangle -A PREROUTING -s 224.0.0.0/3 -j DROP
  iptables -t mangle -A PREROUTING -s 169.254.0.0/16 -j DROP
  iptables -t mangle -A PREROUTING -s 172.16.0.0/12 -j DROP
  iptables -t mangle -A PREROUTING -s 240.0.0.0/5 -j DROP
  iptables -t mangle -A PREROUTING ! -i lo -s 127.0.0.0/8 -j DROP

  #Block Uncommon MSS Values
  #iptables -t mangle -A PREROUTING -p tcp -m conntrack --ctstate NEW -m tcpmss ! --mss 536:65535 -j DROP

  #Block Packets With Bogus TCP Flags
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,SYN FIN,SYN -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,ACK FIN -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,URG URG -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,FIN FIN -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,PSH PSH -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL ALL -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL NONE -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL FIN,PSH,URG -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL SYN,FIN,PSH,URG -j DROP
  iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP

  #Drop fragmented packets
  iptables -t mangle -A PREROUTING -f -j DROP


  if [ "$1" = "init" ]; then
      # not in mangle
      #Max 80 connections per host
      iptables -A INPUT ! -i eth0 -p tcp -m connlimit --connlimit-above 80 -j REJECT --reject-with tcp-reset

      #Limit connections per second
      iptables -A INPUT -p tcp -m conntrack --ctstate NEW -m limit --limit 60/s --limit-burst 20 -j ACCEPT
      iptables -A INPUT -p tcp -m conntrack --ctstate NEW -j DROP

      #Limit reset packets per second
      iptables -A INPUT -p tcp --tcp-flags RST RST -m limit --limit 2/s --limit-burst 2 -j ACCEPT
      iptables -A INPUT -p tcp --tcp-flags RST RST -j DROP

      iptables -A INPUT -i eth1 -p icmp --icmp-type echo-request -j DROP
      iptables -A INPUT -i eth2 -p icmp --icmp-type echo-request -j DROP

      #REMOVE ME
      iptables -A INPUT -s 192.168.0.1 -j DROP
      iptables -A FORWARD -s 192.168.0.1 -j DROP

     fi
}

function firewallinit {
  #Enable IP Forwarding
  echo 1 > /proc/sys/net/ipv4/ip_forward

  len=${#RTARGETS[@]}
    for (( i=0; i<${len}; i++))
      do
        ip route add ${RTARGETS[$i]} via ${RGATES[$i]} metric 1 #Add gateway on default table
      done

  #Flush existing firewall rules
  iptables -F
  iptables -t nat -F
  iptables -t mangle -F

  #FTP Forward modules
  modprobe ip_conntrack_ftp ports=21,1045,2096
  modprobe ip_nat_ftp ports=21,1045,2069

  #GRE Connection Tracking
  modprobe nf_nat_proto_gre
  modprobe nf_conntrack_proto_gre
  modprobe nf_conntrack_pptp
  modprobe nf_nat_pptp

  #Accept local
  iptables -I INPUT -i lo -j ACCEPT

  #Accept some ports
  iptables -A INPUT -p 47 -j ACCEPT
  iptables -A INPUT -p tcp --dport 22005 -j ACCEPT

  #IPORTS
  len=${#IPORTS[@]}
  for (( i=0; i<${len}; i++))
    do
     iptables -A INPUT -p ${IPROTO[$i]} --dport ${IPORTS[$i]} -j ACCEPT
    done

  #APORTS
  len=${#APORTS[@]}
  for (( i=0; i<${len}; i++))
    do
     iptables -A INPUT -p ${APROTO[$i]} --dport ${APORTS[$i]} -j ACCEPT
    done

  iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

  #Block others (not working for now)
  #iptables -A INPUT -i $IF1 -j DROP
  #iptables -A INPUT -i $IF0_2 -j DROP

  #Drop not known IP forwarding
  iptables -P FORWARD ACCEPT

  #IP Forwarding - accept established connections
  iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
  #Internal interface (ACCEPT ON)
  iptables -A INPUT -i $IF0 -j ACCEPT
  iptables -A INPUT -i ppp+ -j ACCEPT

  #Internal interfaces (FORWARD ON)
  iptables -A FORWARD -i $IF0 -j ACCEPT
  iptables -A FORWARD -i $TUN -j ACCEPT
  iptables -A FORWARD -i ppp+ -j ACCEPT
  #LXC Bridge
  iptables -A FORWARD -i $LXCBR -j ACCEPT
  #VPN PORTS
  iptables -A INPUT -p 47 -j ACCEPT
  iptables -A FORWARD -p 47 -j ACCEPT

  #IP Masquerading
  iptables -t nat -A POSTROUTING -o $IF1 -j MASQUERADE
  iptables -t nat -A POSTROUTING -o $IF0 ! -d 192.168.0.0/16 -j MASQUERADE

  #DNS to local server
  iptables -t nat -A PREROUTING -i $IF0 -p udp --dport 53 -j DNAT --to-destination 192.168.0.20:53

  #TO two outside our addresses
  iptables -t nat -A PREROUTING -s 192.168.0.0/16 -d 212.50.15.146 -j DNAT --to-destination 192.168.0.20
  iptables -t nat -A PREROUTING -s 192.168.0.0/16 -d 78.128.66.73 -j DNAT --to-destination 192.168.0.20

  #Connection tracking statements
  #Input packets from if1 & if0_2
  iptables -t mangle -A PREROUTING -i $IF1 -m conntrack --ctstate NEW -j CONNMARK --set-mark 1
  iptables -t mangle -A PREROUTING -i $IF0 ! -s 192.168.0.0/16 -m conntrack --ctstate NEW -j CONNMARK --set-mark 2

  #Output packets to if1 & if0_2
  iptables -t mangle -A POSTROUTING -o $IF1 -m conntrack --ctstate NEW -j CONNMARK --set-mark 1
  iptables -t mangle -A POSTROUTING -o $IF0 ! -s 192.168.0.0/16 -m conntrack --ctstate NEW -j CONNMARK --set-mark 2

  #Preserve connection marks
  iptables -t mangle -A PREROUTING -i $IF0 -m conntrack --ctstate ESTABLISHED,RELATED -j CONNMARK --restore-mark
  iptables -t mangle -A PREROUTING -i $IF0_1 -m conntrack --ctstate ESTABLISHED,RELATED -j CONNMARK --restore-mark
  iptables -t mangle -A PREROUTING -i $LXCBR -m conntrack --ctstate ESTABLISHED,RELATED -j CONNMARK --restore-mark
  iptables -t mangle -A PREROUTING -i $TUN -m conntrack --ctstate ESTABLISHED,RELATED -j CONNMARK --restore-mark
  #Preserve connection mark - local traffic
  iptables -t mangle -A PREROUTING -s 127.0.0.0/24 -m conntrack --ctstate ESTABLISHED,RELATED -j CONNMARK --restore-mark
  iptables -t mangle -A OUTPUT -m conntrack --ctstate ESTABLISHED,RELATED -j CONNMARK --restore-mark

  #Start frukto old ipconfig
  /home/soft/internet-start
 }

function rinit {
    #flush existing routes in tables T1,T2
    ip route flush table T1
    ip route flush table T2
    #Clear all rules except standard ones
    ip rule show | grep -Ev '^(0|32766|32767):' | while read PRIO RULE; do
	  ip rule del prio ${PRIO%%:*} $( echo $RULE | sed 's|all|0/0|' )
	 done

    #Init table T1
    ip route add $P1_NET dev $IF1 src $IP1 table T1
    ip route add default scope global via $GW1 dev $IF1 table T1
    #Init table T2
    ip route add $P0_2_NET dev $IF0_2 src $IP0_2 table T2
    ip route add default scope global via $GW2 dev $IF0_2 table T2

    #default table
    ip route add $P1_NET dev $IF1 src $IP1
    ip route add $P0_2_NET dev $IF0_2 src $IP0_2

    ip rule add fwmark 1 table T1
    ip rule add fwmark 2 table T2
    ip rule add from $IP1 table T1
    ip rule add from $IP0_2 table T2

    #Static routes T1 / Local Routes
    ip route add $P0_NET dev $IF0 table T1
    ip route add $P0_2_NET dev $IF0_2 table T1
    ip route add $TUN_NET dev $TUN table T1
    ip route add $P0_NET0 via $IP0_0 dev $IF0_0 table T1
    ip route add $P0_1_NET via $IP0_1 dev $IF0_1 table T1
    ip route add 127.0.0.0/8 dev lo table T1
    #LXC bridge
    ip route add $LXCBR_NET dev $LXCBR table T1

    len=${#RTARGETS[@]}
    for (( i=0; i<${len}; i++))
      do
        ip route add ${RTARGETS[$i]} via ${RGATES[$i]} metric 1 table T1 #Add gateway on default table
      done

    #Static routes T2 / Local Routes
    ip route add $P0_NET dev $IF0 table T2
    ip route add $P1_NET dev $IF1 table T2
    ip route add $TUN_NET dev $TUN table T2
    ip route add $P0_NET0 via $IP0_0 dev $IF0_0 table T2
    ip route add $P0_1_NET via $IP0_1 dev $IF0_1 table T2
    ip route add 127.0.0.0/8 dev lo table T2
    #LXC bridge
    ip route add $LXCBR_NET dev $LXCBR table T2

    len=${#RTARGETS[@]}
    for (( i=0; i<${len}; i++))
      do
        ip route add ${RTARGETS[$i]} via ${RGATES[$i]} metric 1 table T2 #Add gateway on default table
      done

    rmulti
 }

#Read current connection state
state=$( cat rmulti.st )
if [ "$state" == "" ];
 then
  state=0
 fi;

case "$1" in
 'start')
    firewallinit
    rinit
    rmulti
    ddosProt init
    echo "$DTIME Started. Routing two gateways."
   ;;

 'fwinit')
    firewallinit
    rmulti
    ddosProt init
    echo "$DTIME Firewall Started. Routing two gateways."
   ;;
 'rmulti')
    if [ "$state" -ne "0" ];
     then
        #Service IP Addresses MARK
    	rmulti
    	ddosProt
        echo 0 > rmulti.st
     fi;
    echo "$DTIME Routing two gateways."
  ;;
 'r1')
    if [ "$state" -ne "1" ];
     then
        r1
        ddosProt
        echo 1 > rmulti.st
     fi;
    echo "$DTIME Routing $IF1."
  ;;
 'r2')
    if [ "$state" -ne "2" ];
     then
        r2
        ddosProt
        echo 2 > rmulti.st
     fi;
    echo "$DTIME Routing $IF0_2."
  ;;
 'gw1')
    gw1
    echo "$DTIME Only Server GW $IF1."
  ;;
 'gw2')
    gw2
    echo "$DTIME Only Server GW $IF0_2."
  ;;
 'policy')
    policy
    echo "$DTIME Applying Policy"
  ;;
 'rpolicy')
    rpolicy
    echo "$DTIME Removing Policy"
  ;;

 *)
  echo "Route Multi Usaage: $0 start|fwinit|rmulti|r1|r2|policy|rpolicy|gw1|gw2"
esac

#ip route add default scope global nexthop via $GW2 dev $IF0_2 weight 1
