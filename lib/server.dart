library server;

import 'dart:io';
import 'dart:async';

import 'package:route/server.dart';
import 'package:route/pattern.dart';
import 'package:hostconf/urls.dart';
import 'package:hostconf/render.dart';
import 'package:hostconf/logins.dart';
import 'package:tools/logger.dart';
import 'package:http_server/http_server.dart';

class ConfServer {
  Timer autoApply;
  static const Duration applyTime = const Duration(minutes: 3); //3minutes

  Map _conf;
  RenderPages _rPages;
  Logins _log = new Logins()
   ..loadUsers();

  void setTime() {
    if (autoApply != null) autoApply.cancel();
    autoApply = new Timer(applyTime, () {
      autoApply.cancel();
      autoApply = null;
      LogData("Auto applying settings (timer) ...");
      applySettings();
    });
  }

  void clearTime() {
    if (autoApply != null) autoApply.cancel();
    autoApply = null;
  }

  Future applySettings() async {
    //Will execute here
    try {
      ProcessResult res = await Process.run("/usr/bin/sudo",["./apply.sh"]);
      LogData("SUDO:"+res.stderr.toString()); //Info comes in error for this tool
      LogData("SUDO:"+res.stdout.toString());
    } catch (err) { LogError("ERROR:"+err.toString()); }
  }


  void serveHome(HttpRequest req) {
    _rPages.serveHome(req);
  }

  void redir(HttpRequest req) {
    _rPages.serveRedir(req);
  }

  void serveEdit(HttpRequest req) {
    //print(req.uri.queryParameters);
    var param = editHostUrl.parse(req.uri.path);
    _rPages.serveHome(req,param);
  }

  Future serveSave(HttpRequest req) async {
    var param = editHostUrl.parse(req.uri.path);
    HttpBodyHandler.processRequest(req).then((HttpBody body) {
      Map data = body.body as Map;
      LogData("User "+_log.checkLogin(req.session["sesId"+reqIp(req)] as String)+
          ": Saving host "+param.toString()+" ->\n "+
          data.toString());
      _rPages.editHost(req, param, data);
      setTime();
    });
  }

  String reqIp(HttpRequest req) {
    List<String> h = req.headers["X-Real-IP"];
    String ip;
    if (h == null) ip = req.connectionInfo.remoteAddress.address;
    else ip = h[0];

    return ip;
  }

  void login(HttpRequest req, [int usrCheck]) => _rPages.serveLogin(req, usrCheck);

  Future doLogin(HttpRequest req) async {
    HttpBodyHandler.processRequest(req).then((HttpBody body) {
      Map data = body.body as Map;
      int usrCheck = _log.checkUser(data["user"] as String, data["pass"] as String, reqIp(req));
      if (usrCheck == 1) {
        String sesId = _log.addLogin(data["user"] as String);
        req.session["sesId"+reqIp(req)] = sesId;
        req.response.redirect(Uri.parse("/"));
        LogData("User login "+(data["user"] as String)+" address "+reqIp(req));
      } else {
        //_log.addUser("admin", "Admin adminov", "maniac1234");
        //_log.saveUsers();
        login(req, usrCheck);

        if (usrCheck == 2) LogData("User:"+(data["user"] as String)+" doesn't exist. address "+reqIp(req));
        if (usrCheck == 0) LogData("Invalid login for user: "+(data["user"] as String)+" address "+reqIp(req));
        if (usrCheck == -1) LogData("Maximum tries time reached for IP:."+reqIp(req));
      }
    });
  }

  Future<bool> authFilter(HttpRequest req) async {
    String username = _log.checkLogin(req.session["sesId"+reqIp(req)] as String);
    if (username != null) return true;
     else {
       loginRedir(req);
       return false;
    }
  }

  void loginRedir(HttpRequest req) {
    req.response.redirect(Uri.parse("/login"));
  }

  Future newPassword(HttpRequest req) async {
    String user = _log.checkLogin(req.session["sesId"+reqIp(req)] as String);
    var param = newPasswordUrl.parse(req.uri.path);
    if (param.length < 2) {
      LogData("User $user, address "+reqIp(req)+" password modify invalid parameter count!");
      req.response.write("Невалиден брой параметри!");
      await req.response.close();
     return;
    }

      String oldPass = param[0];
      String newPass = param[1];

      int usrCheck = _log.checkUser(user, oldPass, reqIp(req));
      if (usrCheck == 1) {
        _log.addUser(user, '', newPass);
        await _log.saveUsers();
        LogData("User $user, address "+reqIp(req)+" new passsword $newPass.");
        req.response.write("Успешно променена парола.");
        await req.response.close();
      } else {
        LogData("User $user, address "+reqIp(req)+" try to modify passsword with $newPass.");
        req.response.write("Проблем при промяна на паролата!");
        await req.response.close();
      }
   }

  void doLogout(HttpRequest req) {
    String user = _log.checkLogin(req.session["sesId"+reqIp(req)] as String);
    LogData("User $user logged out. Address "+reqIp(req));
    _log.delLogin(req.session["sesId"+reqIp(req)] as String ?? "");
    loginRedir(req);
  }

  Future doReload(HttpRequest req) async {
    await _rPages.loadSettings();
    req.response.redirect(Uri.parse("/"));
  }

  Future applySets(HttpRequest req) async {
    LogData("User "+_log.checkLogin(req.session["sesId"+reqIp(req)] as String)+
        ": Applying host settings.");
    clearTime(); //Clear timer --> settings are applied
    applySettings();
    req.response.redirect(Uri.parse("/"));
  }

  void delHost(HttpRequest req) {
    var param = delHostUrl.parse(req.uri.path);
    _rPages.delHost(param);
    LogData("User "+_log.checkLogin(req.session["sesId"+reqIp(req)] as String)+
        ": Deleting host "+param.toString());
    req.response.redirect(Uri.parse("/"));
  }

  Future editRedir(HttpRequest req) async {
    var param = editRedirUrl.parse(req.uri.path);
    req.response.write(await _rPages.editRedirUrl(param[0], param[1], param[2], param[3]));
    req.response.close();
    setTime(); //Schedule apply settings
  }

  Future delRedir(HttpRequest req) async {
    List<String> param = delRedirUrl.parse(req.uri.path);
    req.response.write(await _rPages.delRedirUrl(param[0], param[1]));
    req.response.close();
    setTime(); //Schedule apply settings
  }

  ConfServer(this._conf) {
    _rPages = new RenderPages(_conf);

    HttpServer.bind(_conf["host"], _conf["port"] as int).then((server) {
      new Router(server)
        ..filter(matchAny(authUrl), authFilter)
        ..serve(homeUrl, method: 'GET').listen(serveHome)
        ..serve(editHostUrl, method: 'GET').listen(serveEdit)
        ..serve(editHostUrl, method: 'POST').listen(serveSave)
        ..serve(loginUrl, method: 'GET').listen(login)
        ..serve(loginUrl, method: 'POST').listen(doLogin)
        ..serve(logoutUrl,method: 'GET').listen(doLogout)
        ..serve(reloadUrl, method: 'GET').listen(doReload)
        ..serve(applyUrl, method: 'GET').listen(applySets)
        ..serve(delHostUrl, method: 'GET').listen(delHost)
        ..serve(redirUrl, method: 'GET').listen(redir)
        ..serve(newPasswordUrl, method: 'POST').listen(newPassword)
        ..serve(editRedirUrl, method: 'POST').listen(editRedir)
        ..serve(delRedirUrl, method: 'POST').listen(delRedir)
      //  ..serve(articleUrl, method: 'GET').listen(serveArticle)
      //..serve(genIdxUrl, method: 'GET').listen(serveGenIdx)
        ..serve(matchAny(statUrls)).listen(_rPages.serveStatic)
        ..defaultStream.listen(_rPages.serveNotFound);
    });

  }


}