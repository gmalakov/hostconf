library urls;

import 'package:route/url_pattern.dart';

final homeUrl = new UrlPattern(r'/');
final loginUrl = new UrlPattern(r'/login');
final logoutUrl = new UrlPattern(r'/logout');
final reloadUrl = new UrlPattern(r'/reload');
final applyUrl = new UrlPattern(r'/applySettings');
final redirUrl = new UrlPattern(r'/ports');
final delRedirUrl = new UrlPattern(r'/delRedir/(.*)/(.*)'); //Delete port redirection
final editRedirUrl = new UrlPattern(r'/editRedir/(.*)/(.*)/(.*)/(.*)'); //Edit port redirection

final editHostUrl = new UrlPattern(r'/editHost/(.*)/(.*)/(.*)'); //Edit a host
final delHostUrl = new UrlPattern(r'/delHost/(.*)/(.*)/(.*)'); // Delete host

final newPasswordUrl = new UrlPattern(r'/newPass/(.*)/(.*)'); //New password

final authUrl = [homeUrl, delHostUrl, editHostUrl,
              logoutUrl, reloadUrl, applyUrl, newPasswordUrl, redirUrl,
              editRedirUrl, delRedirUrl];

final jsUrl = new UrlPattern(r'/js/(.*)');
final cssUrl = new UrlPattern(r'/css/(.*)');
final imgUrl = new UrlPattern(r'/img/(.*)');
final img2Url = new UrlPattern(r'/images/(.*)');

final statUrls = [jsUrl, cssUrl, imgUrl, img2Url];
