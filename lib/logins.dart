library logins;

import 'dart:math';
import 'dart:async';
import 'package:convert/convert.dart';
import 'package:tools/tools.dart';
import 'package:tools/conf_reader.dart';

class Logins {
  static const int resetTime = 10; //minutes
  static const int tryTimes = 5; //How many times to try for 10 minutes

  Map _users;
  ConfReader _cR;
  Map<String, String> _logins = new Map();

  Map<String, DateTime> _lastCheck = new Map();
  Map<String, int> _checkTimes = new Map();

  void addUser(String name, String fullName, String password) =>
      _users[name] =  <String, String>{"password" : Pass.pwEncode(password), "fullName" : fullName };


  bool checkTimes(String ip) {

    //Initialize variables and return true
    if (_lastCheck[ip] == null) {
      _checkTimes[ip] = 1; _lastCheck[ip] = new DateTime.now();
      return true;
    }

    //We checked before more than 10 minutes
    if (new DateTime.now().difference(_lastCheck[ip]).inMinutes > resetTime) {
      _checkTimes[ip] = 1;
      _lastCheck[ip] = new DateTime.now();
      return true;
    }

    //What is left is we checked before less than 10 minutes
    _checkTimes[ip]++;
    _lastCheck[ip] = new DateTime.now();

    return (_checkTimes[ip] < tryTimes); // Return if we have less than max tries
  }

  int checkUser(String name, String password, String ip) {
    if (!checkTimes(ip))
      return -1; //If we have much tries for this ip return false

      if ((_users == null) || (_users[name] == null))
        return 2;

      bool ret = Pass.pwCheck(password, _users[name]["password"] as String);
      if (ret) resetTimes(ip);
      return (ret) ? 1 : 0;
    }

  void resetTimes(String ip) {
    _lastCheck.remove(ip);
    _checkTimes.remove(ip);
  }

  String addLogin(String name) {
    if ((_users == null) || (_users[name] == null)) return null;

    List<int> rnd = new List();
    var seed = new Random();
    for (int i = 0; i < 20; i++) rnd.add(seed.nextInt(255));
    String hash = hex.encode(rnd);

    _logins[hash] = name;
    return hash;
  }

  void delLogin(String sesId) => _logins.remove(sesId);

  String checkLogin(String sesId) => _logins[sesId];

  Future loadUsers() async {
    try {
      _cR = new ConfReader('../conf/users.conf');
      await _cR.readFile();
      _users = _cR.params;
    } catch (err) {
      //File not exists
      _users = new Map<String, dynamic>();
    }
  }

  Future saveUsers() async {
    await _cR.writeFile();
  }

}