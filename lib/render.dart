library render;

import 'dart:io';
import 'dart:async';

import 'package:IMAP/mime_types.dart';
import 'package:tools/logger.dart';
import 'package:forms/forms.dart';
import 'package:tools/conf_reader.dart';

class HostDef {
  String net;
  String subNet;
  String name;
}

class RenderPages {
  static const String templatePath = "../web/templates";
  static const List<String> identities = ['IPAREA1', 'IPAREA2', 'IPDYN'];
  DHCPConf _dhcp;
  SMBReader _smb;
  SHReader  _sh;
  SHReader _prt;
  Map _conf;

  void setFuncs() {
    FileMaps.instance.fPath = templatePath;

    DataMaps.instance.setFunc("srv404", (Map<String, dynamic> opt) {
      return new Future.value(opt);
    });

    DataMaps.instance.setFunc("login", (Map<String, dynamic> opt) {
      return new Future.value(opt);
    });

    DataMaps.instance.setFunc("tree", (Map opt) {
      Map<String, dynamic> retMap = new Map<String, dynamic>();
      List<Map> networks = new List(); //create networks list
      retMap["networks"] = networks; //add it to return map

      _dhcp.networks.forEach((netName, NetWork val) {
        Map<String, dynamic> cMap = new Map<String, dynamic>();
        networks.add(cMap);
        cMap["netName"] = netName;

        List<Map> subnets = new List(); //Create subnet list
        cMap["subnets"] = subnets; //add it to return map

        _dhcp.networks[netName].subNets.forEach((subNet, SubNet val) {
          Map<String, dynamic> cMap = new Map<String, dynamic>();
          subnets.add(cMap);
          cMap["subName"] = subNet;

          List<Map> hosts = new List(); //Create hosts list
          cMap["hosts"] = hosts; //Add it to return map

          _dhcp.networks[netName].subNets[subNet].hosts.forEach((hostName, Host val) {
            Map<String, dynamic> cMap = new Map<String, dynamic>();
            hosts.add(cMap);

            cMap["hostName"] = hostName;
            cMap["ip"] = val.ip;
            cMap["mac"] = val.mac;
          });
          hosts.sort((a,b) => (Host.ipnum(a["ip"] ?? '').compareTo(Host.ipnum(b["ip"] ?? ''))));
        });
       //subnets.sort((a,b) => (a["subName"].hashCode > b["subName"].hashCode));
      });

      //networks.sort((a,b) => (a["netName"].hashCode > b["netName"].hashCode));
      return new Future.value(retMap);
    });

    DataMaps.instance.setFunc("reDir", (Map<String, dynamic> opt) async {
      //_dhcp.networks[netName].subNets[subName].hosts[hostName];
      //Create full hosts list for all inner subnets
      List<HostDef> hosts = new List();
      for (String net in _dhcp.networks.keys) {
        for (String sub in _dhcp.networks[net].subNets.keys) {
          for (String hName in _dhcp.networks[net].subNets[sub].hosts.keys.toList()..sort()) {
            HostDef host = new HostDef()
              ..net = net
              ..subNet = sub
              ..name = hName;
            hosts.add(host);
          }
        }
      }

      Map<String, dynamic> oMap = new Map<String, dynamic>();
      oMap["hosts"] = hosts.map((el) {
        return <String, dynamic>{
          "name": el.net+'-'+el.name+' '+
            _dhcp.networks[el.net].subNets[el.subNet].hosts[el.name].ip,
          "ip": _dhcp.networks[el.net].subNets[el.subNet].hosts[el.name].ip
        };
      }).toList();

      //Find hostname for ip
      HostDef getHost(String ip) {
        for (HostDef host in hosts)
          if (_dhcp.networks[host.net].subNets[host.subNet].hosts[host.name].ip == ip)
            return host;
        return null;
      }

      List<String> ips = (_prt.vars["ip"] as List<dynamic>).cast<String>();
      List<Map> redirected = new List(); //Init redirected list
      for (int i = 0; i < ips.length; i++) {
        String ip = ips[i];
        HostDef hd = getHost(ip);
        redirected.add(<String, dynamic> {
          "name": (hd?.net ?? "")+'-'+(hd?.name ?? "")+' '+ip,
          "ip": ip,
          "ctr": i,
          "proto": _prt.vars["proto"][i],
          "iport": _prt.vars["iport"][i],
          "oport": _prt.vars["oport"][i],
          "istcp": (_prt.vars["proto"][i] == "tcp"),
          "isudp": (_prt.vars["proto"][i] == "udp"),
        });
      }
      oMap["redirected"] = redirected;

      return oMap;
    });

    DataMaps.instance.setFunc("cHost", (Map<String, dynamic> opt) async {
      Map<String, dynamic> oMap = new Map<String, dynamic>();
      Host host;

      if ((opt["par"] == null) || (opt["par"] is String)) return <String, dynamic>{};
      oMap["hostSel"] = true;

      List<String> par = (opt["par"] as List<dynamic>).cast<String>();
      String netName = par[0];
      String subName = par[1];
      String hostName = par[2]; //Decode parameters

      if (netName.isEmpty) return <String, dynamic>{};
      if (subName.isEmpty) return <String, dynamic>{};

      if (hostName.isNotEmpty) {
        host = _dhcp.networks[netName].subNets[subName].hosts[hostName];
        oMap["name"] = hostName;
        oMap["ip"] = host?.ip;
        oMap["mac"] = host?.mac;
      }


      oMap["freeIps"] = (_dhcp.networks[netName].subNets[subName].availableIps().map((el) => {"ip": el}));


      //internet configuration
      List<Map> categories = new List();
      oMap["categories"] = categories;
      _sh.vars.forEach((String name, dynamic val) {
        if (identities.contains(name)) {
          bool hasIp = false;

          if (val is String) val = new List<String>()
            ..add(val as String);

          if (val is List) {
            for (int i = 0; (i < val.length) && (!hasIp); i++)
              if (val[i] == host?.ip) hasIp = true;

            categories.add(<String, dynamic>{
              "catName": name,
              "selected": hasIp
            });
          }
        }
      });

      //Samba configuration
      List<Map> folders = new List();
      oMap["folders"] = folders;
      _smb.sections.forEach((key, val) {
        if (key != "global") {
          //It's a folder
          dynamic ips = val["hosts allow"];
          if (ips == null) ips = new List<String>(); //Initialize value
          if (ips is String) {
            if ((ips as String).isNotEmpty) ips = new List<String>()..add(ips as String);
            else ips = new List<String>();
          }

          if (ips is List) {
            bool hasIp = false;

            for (int i = 0; (i < ips.length) && (!hasIp); i++)
               if (ips[i] == host?.ip) hasIp = true;

            folders.add(<String, dynamic>{
              "folder": key,
              "selected": hasIp
            });
          }
        }
      });

      return oMap;
    });
   }

    Future<String> renderNotFound([Map<String,String> par]) {
    DataMaps.instance.param = par;

    BasicRender render = new BasicRender('page404.yaml');
    return render.render;
  }

  void serveNotFound(HttpRequest req) {
    renderNotFound(<String, String>{"path": req.uri.path}).then((String res) {
      String mimeType = 'text/html; charset=UTF-8';
      req.response.headers.set('Content-Type', mimeType);
      req.response.statusCode = HttpStatus.notFound;

      req.response.write(res);
      req.response.close();
    });
  }

  Future serveLogin(HttpRequest req, [int usrCheck]) async {
    if (usrCheck == null) usrCheck = 1; //If not defined then no error will be displayed
    DataMaps.instance.param = <String, dynamic>{"path": req.uri.path,
      "loginErr" : ((usrCheck == 0) || (usrCheck == 2)), "triesErr": (usrCheck == -1) };
    BasicRender render = new BasicRender('login.yaml');
    String res = await render.render;

    req.response.headers.set('Content-Type', 'text/html; charset=UTF-8');
    req.response.writeln(res);
    req.response.close();
  }

  Future serveHome(HttpRequest req, [dynamic par]) async {
    DataMaps.instance.param = <String, dynamic>{"path": req.uri.path, "par": par};
    BasicRender render = new BasicRender('layout.yaml');
    String res = await render.render;

    req.response.headers.set('Content-Type', 'text/html; charset=UTF-8');
    req.response.writeln(res);
    req.response.close();

  }

  Future serveRedir(HttpRequest req, [dynamic par]) async {
    DataMaps.instance.param = <String, dynamic>{};
    BasicRender render = new BasicRender('ports.yaml');
    String res = await render.render;

    req.response.headers.set('Content-Type', 'text/html; charset=UTF-8');
    req.response.writeln(res);
    req.response.close();
  }

  Future<String> delRedirUrl(String ip, String oport) async {
    if (ip.isNotEmpty) {
      prtDelVar(ip, oport);
      await _prt.writeFile();
      return "OK";
    }
    else return "Ip is empty!";
  }

  Future<String> editRedirUrl(String ip, String oport, String iport, String proto) async {
    if (ip.isNotEmpty) {
      if (ip == "new") return "Хоста е невалиден!";
      //Find index
      int usedIdx = -1;
      List<dynamic> tmp = _prt.vars["oport"] as List<dynamic>;
      int i = 0;
      do {
        //print("$i:"+tmp[i]+"-"+oport);
        if (tmp[i] == oport) usedIdx = i; //Found the port
        i++;
      } while (i < tmp.length && (tmp[i] != oport || _prt.vars["ip"][i] != ip));
      if (i < tmp.length) usedIdx = i; //Index is found

      int idx = -1;
      if (usedIdx > -1 && _prt.vars["ip"][usedIdx] == ip) idx = usedIdx;
       else
        //It's not our index so find the idx we are looking for
        idx = (_prt.vars["ip"] as List<dynamic>).cast<String>().indexOf(ip);

      //print("$usedIdx/$idx $ip $oport $iport $proto");

      //Same port is used on same protocol and it's not our record
      if ((usedIdx > -1) && (idx != usedIdx) && (_prt.vars["proto"][usedIdx] == proto)) return "Порта е вече зает!";

      //Same ip different port number -> then will be new port forward
      if (idx > -1 && _prt.vars["oport"][idx] != oport) idx = -1;

      if (idx == -1) {
        //Adding new port mapping
        _prt.vars["ip"].add(ip);
        _prt.vars["oport"].add(oport);
        _prt.vars["iport"].add(iport);
        _prt.vars["proto"].add(proto);
      } else {
        //Already exists just change port numbers
         // _prt.vars["oport"][idx] = oport; -> Outport shoudn't be modified we are comparing trough it
        _prt.vars["iport"][idx] = iport;
        _prt.vars["proto"][idx] = proto;
      }

      LogData('Port forward edit: $ip<$oport -> $iport / $proto>');

      await _prt.writeFile();//Write changes back
      return "OK";
    } return "Хоста е празен!";
  }

  Future editHost(HttpRequest req, List param, Map data) async {
    Host host;
    if ((param[2] as String).isNotEmpty) {
      host = _dhcp.networks[param[0]].subNets[param[1]].hosts[param[2]];

      //Delete old ip
      shDelVar(host.ip);
      smbDelVar(host.ip);
      //Change port forwarding ip into place
      prtChgVar(host.ip, data["ipval"] as String);

      host.name = data["nameval"] as String;
      host.ip = data["ipval"] as String;
      host.mac = (data["macval"] as String).replaceAll("-", ":");

      //Remove old name
      _dhcp.networks[param[0]].subNets[param[1]].hosts.remove(param[2]);
    } else host = new Host(data["nameval"] as String, data["ipval"] as String, data["macval"] as String);

    //Add with new name
    _dhcp.networks[param[0]].subNets[param[1]].hosts[host.name] = host;

    //SH
    String shVal = data["catval"] as String;
    if ((shVal != null) && (shVal.isNotEmpty)) {
      dynamic shv = _sh.vars[data["catval"]];
      if (shv is String) shv = new List<String>()..add(shv as String);
      if (shv is List) shv.add(host.ip); //Add new ip to selected category
      _sh.vars[data["catval"] as String] = shv; //Put it back into place
    }

    //SMB
    _smb.sections.forEach((name, vars) {
      dynamic ips = vars["hosts allow"];
      if (ips == null) ips = new List<String>(); //Initilize value
      if (ips is String) {
        if ((ips as String).isNotEmpty) ips = new List<String>()..add(ips as String);
         else ips = new List<String>();
      }

      if (ips is List) {
        //all good start adding
        if (data[name+"_cb"] != null) {
          //Folder is set
          ips.add(host.ip); //Set ip there
        }

        //Put data back into place
        //print(ips.length);
        if (ips.length >  0) vars["hosts allow"] = ips;
         else vars.remove("hosts allow");
      }
    });

    //Write file back
    await _dhcp.writeFile();
    await _sh.writeFile();
    await _smb.writeFile();
    await _prt.writeFile();

    serveHome(req);
  }

  void serveStatic(HttpRequest req, {String path}) {
    String MY_HTTP_ROOT_PATH;
    if (path == null) {
      path = req.uri.path; // if called with no path specified;
      MY_HTTP_ROOT_PATH = Platform.script.resolve('../web').toFilePath();
    } else MY_HTTP_ROOT_PATH = Platform.script.resolve('./web').toFilePath();
    //Start with current path if path is put by parameter

    File file = new File(MY_HTTP_ROOT_PATH + path);
    file.exists().then((bool found) {
      String mimeType;
      try {
        mimeType = Mime.mime(path);
      } catch (err) { }
      if (mimeType == null) mimeType = 'text/plain; charset=UTF-8';
      req.response.headers.set('Content-Type', mimeType);
      if (found) {
        file.openRead()
            .pipe(req.response) // HttpResponse type.
            .catchError((dynamic e) => LogError(e.toString()));
      } else {
        LogData("Content required was not found: $path");
        serveNotFound(req);
      }
    });
  }

  void shDelVar(String ip) {
    _sh.vars.forEach((key, dynamic el) {
      if (el is String) el = new List<String>()..add(el as String);
      if (el is List) {
        el.remove(ip);
        _sh.vars[key] = el;
      }
    });
  }

  void prtChgVar(String oldIp, String newIp) {
    dynamic el = _prt.vars["ip"];
    if (el is String) el = new List<String>()..add(el as String);
    if (el is List) {
      int i = el.indexOf(oldIp);
      if (i > -1) {
        el[i] = newIp; //Change ip into place keeping all other vars the same
        _prt.vars['ip'] = el;
      }
    }
  }

  void prtDelVar(String ip, String oport) {
    dynamic el = _prt.vars["ip"];
    if (el is String) el = new List<String>()..add(el as String);
    if (el is List) {
      for (int i = 0; i < el.length; i++) {
        //Delete from ports if oport == -1 or ip // oport are suitable
        if (el[i] == ip && (oport == "-1" || _prt.vars['oport'][i] == oport)) {
          el.removeAt(i);
          _prt.vars['ip'] = el;
          _prt.vars['iport'].removeAt(i);
          _prt.vars['oport'].removeAt(i);
          _prt.vars['proto'].removeAt(i);
        }
      }
    }
  }

  void smbDelVar(String ip) {
    _smb.sections.forEach((key, el) {
      dynamic hosts = el["hosts allow"];
      if (hosts is String) hosts = new List<String>()..add(hosts as String);
      if (hosts is List) {
        hosts.remove(ip);
        el["hosts allow"] = hosts;
      }
    });
  }


  Future delHost(List par) async {
    Host host = _dhcp.networks[par[0]].subNets[par[1]].hosts[par[2]];
    _dhcp.networks[par[0]].subNets[par[1]].delHost(par[2] as String);
    shDelVar(host.ip);
    smbDelVar(host.ip);
    prtDelVar(host.ip, "-1");

    await _dhcp.writeFile();
    await _sh.writeFile();
    await _smb.writeFile();
    await _prt.writeFile();
  }

  Future loadSettings() async {
    //Init settings
    _dhcp = new DHCPConf(_conf["dhcp"] as String);
    await _dhcp.readFile();
//    print(_dhcp.networks["eth0"].subNets.keys);

    _sh = new SHReader(_conf["inet"] as String);
    await _sh.readFile();

    _smb = new SMBReader(_conf["smb"] as String);
    await _smb.readFile();

    _prt = new SHReader(_conf["ports"] as String);
    await _prt.readFile();

    //Init file if needed
    _prt.vars["ip"] = _prt.vars["ip"] ?? new List<String>();
    _prt.vars["iport"] = _prt.vars["iport"] ?? new List<String>();
    _prt.vars["oport"] = _prt.vars["oport"] ?? new List<String>();
    _prt.vars["proto"] = _prt.vars["proto"] ?? new List<String>();
  }

  RenderPages(this._conf) {
    setFuncs();
    loadSettings();
  }

}