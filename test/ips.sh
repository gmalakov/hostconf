#!/bin/bash

#Network interfaces
#Openvpn tunnel
TUN=( "tun0" )
IF1=( "eth1" )
IF0=( "eth0" )
IF0_0=( "eth0:0" )
IF0_1=( "eth0:1" )
IF0_2=( "eth0:2" )
LXCBR=( "lxcbr0" )

#IPs of the def gateways
GW1=( "192.168.1.1" )
GW2=( "192.168.88.1" )

#Static routes IP/MASK gateway IP
RTARGETS=( "192.168.0.64/26" )
RGATES=( "10.8.8.2" )

APORTS=( "80" "443" "22005" )
APROTO=( "tcp" "tcp" "tcp" )

#IPArea for ETH2 - default BTK (Support IP or IPArea)
IPAREA1=( "192.168.5.2-192.168.5.253" )
IPAREA2=( "192.168.0.1-192.168.0.21" "192.168.0.22" "192.168.0.23-192.168.0.119" )
#Iparea for dynamic ips -> Load ballancing
IPDYN=( "192.168.254.31-192.168.254.220" "192.168.0.14" )

