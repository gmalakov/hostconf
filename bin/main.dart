import 'dart:async';
import 'package:hostconf/server.dart';
import 'package:tools/conf_reader.dart';
import 'package:tools/logger.dart';
//import 'package:hostconf/logins.dart';

Future main(List<String> args) async {
  /*var log = new Logins();
  await log.loadUsers();
  log.addUser('admin', 'Admin adminov', 'xxxxx');
  await log.saveUsers();*/

  //Read file paths
  ConfReader _cR = new ConfReader('../conf/hosts.conf');
  await _cR.readFile();
  Map conf = _cR.params;

  runZoned(() {
    new ConfServer(conf);
  }, onError: (dynamic e,dynamic s) {
    LogError("EXCEPTION: $e, $s");
  });

}
