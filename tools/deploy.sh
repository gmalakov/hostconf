#!/bin/bash

web=../web
bin=../bin
tools=../tools
dpath=/home/soft/dart/dart-sdk/bin

tuser=user
taddress=linux.fructo.bg
tfolder=/home/user/hostconf

target="$tuser@$taddress:$tfolder"
port=22005

#Create snapshot
echo "Creating snapshot ..."
cd $bin
./build.sh
cd $tools

echo "Copying data to server ..."
#Copy all web folders
scp -P $port $web/js/* $target/web/js
scp -P $port $web/css/* $target/web/css
scp -P $port $web/templates/* $target/web/templates

#Copy snapshot to server
scp -P $port $bin/main.snapshot $target/bin
#Copy startups
scp -P $port $bin/st*sh $target/bin

echo "Checking DART version ..."
#check local dart version
dart --version 2>./tmp.tmp
lversion=$(cat tmp.tmp)
rm -rf tmp.tmp
ssh -t -p $port $tuser@$taddress "$dpath/dart --version" 1>tmp2.tmp
rversion=$(cat tmp2.tmp)
rm -rf tmp2.tmp

lvarr=( $lversion )
rvarr=( $rversion )

#echo "versions"
#echo ${lvarr[3]}
#echo ${rvarr[3]}

if [ "${lvarr[3]}" != "${rvarr[3]}" ];
 then
   echo "Dart version mismatch. Local version is ${lvarr[3]}, remote is ${rvarr[3]} ."
   echo 'Updating dart ...'
   #copy update path
   scp -P $port dupdate.sh root@$taddress:/home/soft/dart
   scp -P $port down.sh root@$taddress:/home/soft/dart

   ssh -t -p $port root@$taddress << HERE
     cd /home/soft/dart
     ./dupdate.sh
HERE
 fi

echo "Restarting process on server ..."
ssh -t -p $port $tuser@$taddress << HERE
  cd $tfolder/bin
  ./stNew.sh
HERE

#Push repository
cd ..
./push.sh
