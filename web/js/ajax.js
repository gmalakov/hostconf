var busyflag = false;
var time_req;
var cs_obj = mkRequestObj();
var last_action = "";

function mkRequestObj() {
    var req_obj;
    var browser = navigator.appName;
    if(browser == "Microsoft Internet Explorer")
        req_obj = new ActiveXObject("Microsoft.XMLHTTP");
    else
        req_obj = new XMLHttpRequest();
    return req_obj
}

function chk_state() {
    var ntime_req = new Date().getTime();
    if (ntime_req - time_req > 10000) {
        cs_obj.onreadystatechange = function() {};
        busyflag = false;
        alert("Изтекло време!");
        return;
    }

    if (cs_obj.readyState == 4) {
        cs_obj.onreadystatechange = function() {};
        busyflag = false;
        var data = cs_obj.responseText;
        if (data != "OK") alert(data); //Bring response text if not OK!
    }
}

function call_server(msg,act) {
    if (busyflag == true) {
        setTimeout("call_server('"+msg+"','"+act+"');", 1000);
        return;
    }

    busyflag = true;
    time_req = new Date().getTime();

    cs_obj.open('post', act);
    cs_obj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    cs_obj.send(msg);
    cs_obj.onreadystatechange = function() { chk_state(); };
    last_action = act;
}
